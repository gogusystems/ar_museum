﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicSlider : MonoBehaviour {


    public AudioSource audioSource;
    public Slider slider;

    // Update is called once per frame
    void Update () {
        slider.value = audioSource.time / audioSource.clip.length;
    }


    public void ChangeAudioTime()
    {
        audioSource.time = audioSource.clip.length * slider.value;
    }

}
