﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebURLScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
    public void ButtonNataschaKohnenYoutube()
    {
        Application.OpenURL("https://www.youtube.com/channel/UC2YrVWMEnK2CVMl0R51oShg");
    }
    public void ButtonNataschaKohnenFacebook()
    {
        Application.OpenURL("https://de-de.facebook.com/Natascha.Kohnen.SPD/");
    }
    public void ButtonNataschaKohnenTwitter()
    {
        Application.OpenURL("https://twitter.com/NataschaKohnen");
    }
    public void ButtonNataschaKohnenInstagram()
    {
        Application.OpenURL("https://www.instagram.com/nataschakohnen/?hl=de");
    }
    public void ButtonAnfahrt()
    {
        Application.OpenURL("https://goo.gl/maps/6yDa4qWjr5q");
    }
    // Update is called once per frame
    void Update () {
		
	}
}
