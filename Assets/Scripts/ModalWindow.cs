﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;

namespace Vuforia
{

    public class ModalWindow : MonoBehaviour
    {
        public Camera cam;

        // Variables for Modal Window
        public Transform PanelInfo;
        public Transform ManualPanel;
        public Transform DownloadPanel;
        public Transform BackVideoPanel;

        public AspectRatioFitter imageFitter;

        //   public Transform ButtonCloseVideoPanel;


        public RawImage imagePlayer;
        //  public RawImage ModalWindow;
        //    public RawImage ModalImage;
        //  HELO BITBUCKET;
        //  HELO gogusystems;


        private string[] galleryPictureStringDE = new string[4] {
         "<b>Fassbrücke und Sunset Hill</b>\nDatum: 1858, Holzstich, Tinte und Farbe auf Papier",
         "<b>Stockente zwischen schneebehangenem Schilf</b>\nDatum:ca.1830, Holzstich, Tinte und Farbe auf Papier",
         "<b>Affenbrücke in der Kay Provinz</b>\nDatum: 1853, Holzstich, Tinte und Farbe auf Papier",
         "<b>Aussicht von der Satta Suruga Provinz</b>\nDatum: 1858, Holzstich, Tinte und Farbe auf Papier"
        };

        private string[] galleryPictureStringEN = new string[4] {
         "<b>Drum Bridge and Sunset Hill, Meguro</b>\nDate: 1858, woodblock print, ink and colors on paper",
         "<b>Mallards Beneath Snow-Laden Reeds</b>\nDate: 1830s, woodblock print, ink and colors on paper",
         "<b>The monkey bridge in the kay province</b>\nDate: 1853, woodblock print, ink and colors on paper",
         "<b>View from Satta Suruga Province</b>\nDate: 1858, woodblock print, ink and colors on paper"
        };

        private string galleryDescriptionStringDE = "<b>Utagawa Hiroshige</b>\n\nEine Zusammenstellung der Holzschnitte des Künstlers.";
        private string galleryDescriptionStringEN = "<b>Utagawa Hiroshige</b>\n\nA composition of the woodblock prints by the artist.";

        private string ManualPanelTextStringDE = "";
        private string ManualPanelTextStringEN = "";

        private string CloseManualTextButtonDE = "Verstanden";
        private string CloseManualTextButtonEN = "Understand";

        private string ThreeDbuttonCloseTextDE = "Schließen";
        private string ThreeDbuttonCloseTextEN = "Close";


        private string[] TitleOfVideoStringDE = new string[3] {
         "<b>Niki de Saint-Phalle</b>\n\nSchießperfomance als Protest gegen die Gewalt in unserer Zeit.",
         "<b>Utagawa Hiroshige</b>\n\nKurzbiografie zu Utagawa Hiroshige",
         "<b>Faustkämpfer vom Quirinal</b>\n\nDer Faustkämpfer vom Quirinal in Rom ist eines von nur sehr wenigen überlieferten griechischen Originalen."
        };
        private string[] TitleOfVideoStringEN = new string[3] {
         "<b>Niki de Saint-Phalle</b>\n\nShooting performance as a protest against the violence of our times.",
         "<b>Utagawa Hiroshige</b>\n\nShort biography about Utagawa Hiroshige",
         "<b>Boxer at rest</b>\n\nThe bronze Boxer at Rest, is a Hellenistic Greek sculpture of a sitting nude boxer at rest."
        };

        private string[] VideoDescriptionLongStringDE = new string[3] {
         "<b>Niki de Saint-Phalle</b>\n\n„Indem ich auf mich selbst schieße, nehme ich Ziel auf die Ungerechtigkeiten in der Gesellschaft. Indem ich auf meine eigene Gewalt schieße, nehme ich Ziel auf die Gewalt in unserer Zeit“, sagt Niki.",
         "<b>Utagawa Hiroshige</b>\n\nKurzbiografie über den Lebensweg Utagawa Hiroshige und die Auseinandersetzung mit seiner Umwelt.",
         "<b>Faustkämpfer vom Quirinal</b>\n\nDer Faustkämpfer vom Quirinal in Rom ist wohl die bemerkenswerteste erhaltene Bronzestatue eines solchen Athleten - und eines von nur sehr wenigen überlieferten griechischen Originalen."
        };
        private string[] VideoDescriptionLongStringEN = new string[3] {
         "<b>Niki de Saint-Phalle</b>\n\n“In shooting myself, I took aim at society and its injustices. In shooting my own violence, I took aim at the violence of the times.“ says Niki.",
         "<b>Utagawa Hiroshige</b>\n\nShort biography about the life of Utagawa Hiroshige and the expression with his surrounding.",
         "<b>Boxer at rest</b>\n\nThe bronze Boxer at Rest, is a Hellenistic Greek sculpture of a sitting nude boxer at rest, still wearing his caestus, a type of leather hand-wrap. The sculpture dates back to 330 to 50 BCE. It was excavated in Rome in 1885."
        };

        private string[] TextDescriptionStringDE = new string[3] {
         "<b>Niki de Saint-Phalle</b>\n\nNiki de Saint-Phalle wird 1930 in Frankreich geboren wächst aber in New York auf. Als 18-Jährige heiratet sie den amerikanischen Schriftsteller Harry Matthews. Sie haben zwei Kinder, die Ehe geht jedoch in die Brüche. Niki erleidet einen psychischen Zusammenbruch und um ihre Traumata und Ängste zu verarbeiten, beginnt sie künstlerisch zu arbeiten. Ab 1951 lebt Niki de Saint Phalle in Paris und lernt hier den Künstler Jean Tinguely kennen. 1971 heiraten Niki de Saint Phalle und Jean Tinguely. Er hilft ihr und inspiriert sie, es entstehen gemeinsame Projekte und durch ihn kommt sie auch in Kontakt zu der Künstlergruppierung der Nouveau Réalistes, die sich im Oktober 1960 zusammenschließt.",
         "<b>Utagawa Hiroshige</b>\n\nThe Sea at Satta in Suruga Province\nSerie: 36 Ansichten des Berg Fuji\nDate: 1858\nFarbiger Holzstich\n\nDer Berg Fuji wird hier umrahmt von riesigen Wellen im Vordergrund. Das Bild ist angelehnt an die berühmten Abbildungen Hokusai's, hauptsächlich bekannt unter dem Namen „Die große Welle“. Hiroshige's Version ist ruhiger und distanzierter. Das Wasser wurde mit großer Raffinesse gedruckt. Es wurden drei verschiedene Blautöne im Kontrast zum weißen Wellenberg gewählt.",
         "<b>Faustkämpfer vom Quirinal</b>\n\nDer Faustkämpfer vom Quirinal in Rom ist wohl die bemerkenswerteste erhaltene Bronzestatue eines solchen Athleten - und eines von nur sehr wenigen überlieferten griechischen Originalen. Die Datierung schwankt zwischen dem späten 4. Jhrdt. v. Chr. und der Mitte des 1. Jhrdt. v. Chr.Die 128 Zentimeter hohe und aus acht Teilstücken zusammengelötete Sitzfigur (der Sockel ist nicht original) wurde im Jahr 1885 auf dem Quirinal entdeckt, wodurch sie ihren Namen bekam. Eine weitere Bronze, die unweit von ihr dort gefunden wurde, ist ein Standmotiv und stellt wohl einen nicht identifizierten hellenistischen Fürsten dar. Möglicherweise gehörte sie zur Ausstattung der dort gelegenen Thermen des Kaisers Konstantin."
        };
        private string[] TextDescriptionStringEN = new string[3] {
         "<b>Niki de Saint-Phalle</b>\n\nNiki de Saint Phalle (29.10.1930 – 21.05.2002)\nwas a French-American sculptor, painter, and filmmaker.\nShe was one of the few women artists widely known for monumental sculpture, but also for her commitments.\nShe had a difficult and traumatic childhood and education, which she wrote about decades later. After an early marriage and two children, she began creating art in a naïve, experimental style. She first received worldwide attention for angry, violent assemblages which had been shot by firearms. These evolved into Nanas, light-hearted, whimsical, colorful, large-scale sculptures of animals, monsters, and female figures. Her most comprehensive work was the Tarot Garden.",
         "<b>Utagawa Hiroshige</b>\n\nThe Sea at Satta in Suruga Province\nSeries: Thirty-six Views of Mount Fuji\nDate: 1858\nColour woodblock print\n\nHere Mount Fuji is framed by a giant curling wave in the foreground. The design recalls Hokusai’s famous depiction of Fuji, commonly known as ‘The Great Wave’. Hiroshige’s version is calmer and more detached. The water has been printed with great sophistication, with three different shades of blue contrasting with the white wave crests, which in turn harmonize with the white peak of Mount Fuji.",
         "<b>Boxer at rest</b>\n\nThe bronze Boxer at Rest, is a Hellenistic Greek sculpture of a sitting nude boxer at rest, still wearing his caestus, a type of leather hand-wrap. The sulpture dates back to 330 to 50 BCE. It was excavated in Rome in 1885, and is now in the collection of the National Museum of Rome. The Boxer at Rest is one of the finest examples of bronze sculptures to have survived from the ancient world; survivals from the period are rare, as they were easily melted down and transformed into new objects. The work comes from a period in Greek art where there is a movement away from idealised heroic depictions of the body and youth, and an exploration of emotional themes and greater realism."
        };

        private string[] AudioTextDescriptionStringDE = new string[3] {
         "<b>Niki de Saint-Phalle: Leben, Kunst und Einfluss</b>\n\nNiki de Saint-Phalle wurde 1931 in Frankreich geboren\nund wuchs in New York auf.\nAngefangen als Modell für das Vogue und Life Magazin zog sie nach einiger Zeit zurück nach Frankreich um sich der Kunst zu widmen.",
         "<b>Utagawa Hiroshige</b>\n\nBiografie über das Lebenswerk von Utagawa Hiroshige",
         "<b>Faustkämpfer vom Quirinal</b>\n\nDer Faustkämpfer vom Quirinal in Rom ist wohl die bemerkenswerteste erhaltene Bronzestatue eines solchen Athleten - und eines von nur sehr wenigen überlieferten griechischen Originalen. Die Datierung schwankt zwischen dem späten 4. Jhrdt. v. Chr. und der Mitte des 1. Jhrdt. v. Chr."
        };
        private string[] AudioTextDescriptionStringEN = new string[3] {
         "<b>Niki de Saint-Phalle: The Life, Art and Influences</b>\n\nNiki de Saint Phalle was born in 1931 in France\nbut grew up in New York.\nShe became a fashion model for vogue and life magazine.\nShe then moved back to France to begin a career\nas a self-taught artist.",
         "<b>Utagawa Hiroshige</b>\n\nBiography about the life's work of Utagawa Hiroshige",
         "<b>Boxer at rest</b>\n\nThe bronze Boxer at Rest, is a Hellenistic Greek sculpture of a sitting nude boxer at rest, still wearing his caestus, a type of leather hand-wrap. The sulpture dates back to 330 to 50 BCE. It was excavated in Rome in 1885, and is now in the collection of the National Museum of Rome."
        };


        public Text ArtworkText;
        public Text GalleryTextDescription;
        public Text TitleOfVideo;
        public Text VideoDescriptionLong;
        public Text TextDescription;
        public Text AudioTextDescription;


        public Text DownloadPanelText;

        public Text ManualPanelText;
        public Text CloseManualTextButton;
        public Text ThreeDbuttonCloseText;

        // Variables for resources files: video, sound , pictures : unity source : streaming source : local file source
        // local_file_source  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // streaming_source
        // unity_source
        private string resources_files = "local_file_source";

        public Texture[] introImages;

        public Texture image_01;
        public Texture image_02;
        public Texture image_03;
        public Texture image_04;
        public Texture image_05;
        public Texture image_06;

        private string image_01_file = "hiroshige_bruecke1086_scaled.jpg";
        private string image_02_file = "hiroshige_ente652_scaled.jpg";
        private string image_03_file = "hiroshige_schlucht972_scaled.jpg";
        private string image_04_file = "hiroshige_wellen.jpg";
        private string image_05_file = "niki_scaled.jpg";
        private string image_06_file = "boxerimage.png";

        private WWW www01;
        private WWW www02;
        private WWW www03;
        private WWW www04;
        private WWW www05;
        private WWW www06;

        //Variables for Video 
        private bool is_youtube = true; // for local disc and streaming from web: 
        public VideoClip video_1;
        public VideoClip video_2;
        public VideoClip video_3;
        private string urlsave = @"C:\goguAR\client02\";
        private string urlsaveAndroid = "/goguAR/client02/";
        private string urlGetfromServer = "https://gogu.gotresor.de/goguAR/client01/";
        private string video_1_file = "01Hiroshige_iphone.mp4";
        private string video_2_file = "01niki_iphone.mp4";
        private string video_3_file = "boxer.mp4";

        private string sound_1_file = "nikiaudio.mp3";
        private string sound_2_file = "Hiroshige_Biography_from_Goodbye-Art_Academy.mp3";
        private string sound_3_file = "boxeraudio.mp3";

        private string sound_1_unity = "sounds/nikiaudio";
        private string sound_2_unity = "sounds/Hiroshige Biography from Goodbye-Art Academy";
        private string sound_3_unity = "sounds/boxeraudio";


        // Controls the Videoplayer
        public VideoPlayerController videoPlayer;

        public VideoPlayer VideoPlayerRaw;

        public Transform PanelVideo;

        public Transform ModalImage;

        //new: by elijas
        public Transform PreviewImage;

        //new: Videobutton and VideoThumbnailWindow
        public Transform VideoThumbnailWindow;

        public Transform VideobuttonGrey;
        public Transform Videobutton;
        public Transform AudiobuttonGrey;
        public Transform Audiobutton;
        public Transform TextbuttonGrey;
        public Transform Textbutton;
        public Transform GallerybuttonGrey;
        public Transform Gallerybutton;
        public Transform ThreeDbuttonGrey;
        public Transform ThreeDbutton;

        public Transform ThreeDbuttonClose;
        public Transform BottomPanel;
        public Transform ThreeDPanel;

        public Transform FullScreenVideoButton;
        public Transform PlayButton;
        public Transform PauseButton;
        public Transform Progress;
        public Transform BackgroundProgres;

        public Transform LeftButtonImage;
        public Transform RightButtonImage;


        public Transform TextPanel;
        public Transform GalleryPanel;
        public Transform GalleryDescriptionPanel;


        public Transform ButtonEnglish;
        public Transform ButtonGerman;

        //public Transform ButtonCloseVideoPanel;
        public Transform ButtonCloseVideoPanel;
        public Transform ButtonCloseImagePanel;
        public Transform CloseButtonGallery;


        public Transform ArtworkImage;
        public Transform ArtworkThumbnail1;
        public Transform ArtworkThumbnail2;
        public Transform ArtworkThumbnail3;
        public Transform ArtworkThumbnail4;


        public Transform ButtonQuestion;



        public Transform CloseManualButton;

        private bool is_ThreeDPanel_close = true;
        private bool Search_Target = true;
        private string Search_Target_active = "";

        private VideoClip current_video;
        private string current_video_url = "";
        //private bool is_youtube = false;

        //new: Audio Variables
        public Transform ButtonShowAudioThumbnailWindow;
        public Transform AudioThumbnailWindow;

        public Transform ButtonPlayStopSound;

        public Sprite butt_play_sprite;
        public Sprite butt_stop_sprite;

        public AudioSource soundTarget;
        public Slider slider;
        private AudioClip clipTarget;

        private string current_sound = "";
        private string current_Language = "De";
        private int current_Picture_gallery = 1;
        private int current_TrackableName = 0;
        private int current_FullScreenVideo = 0;


        private bool sound_plays = false;
        private bool video_plays = false;
        private bool show_VideoPanel = false;



        private IEnumerator _downloadResourceAndSave(WWW www, string savePath, bool closedownload)
        {
            yield return www;

            //Check if we failed to send
            if (string.IsNullOrEmpty(www.error))
            {
                //  Debug.Log("Success");

                //Save Image
                saveImage(savePath, www.bytes);
                Debug.Log("FILE: " + savePath);
            }
            else
            {
                //  Debug.Log("Error: " + www.error);
            }
            if (closedownload)
            {
                CloseDownloadPanel();
                if (resources_files == "local_file_source")
                { //local_file_source
#if UNITY_EDITOR
                    www01 = new WWW(urlsave + "pictures\\" + image_01_file);
                    www02 = new WWW(urlsave + "pictures\\" + image_02_file);
                    www03 = new WWW(urlsave + "pictures\\" + image_03_file);
                    www04 = new WWW(urlsave + "pictures\\" + image_04_file);
                    www05 = new WWW(urlsave + "pictures\\" + image_05_file); // niki
                    www06 = new WWW(urlsave + "pictures\\" + image_06_file); // boxer

#elif UNITY_ANDROID || UNITY_IOS
                 www01 = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "pictures/" + image_01_file);
                 www02 = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "pictures/" + image_02_file);
                 www03 = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "pictures/" + image_03_file);
                 www04 = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "pictures/" + image_04_file);
                 www05 = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "pictures/" + image_05_file); // niki
                 www06 = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "pictures/" + image_06_file); // boxer
#endif
                }
                else
                {//streaming_source
                    www01 = new WWW(urlGetfromServer + "pictures/" + image_01_file);
                    www02 = new WWW(urlGetfromServer + "pictures/" + image_02_file);
                    www03 = new WWW(urlGetfromServer + "pictures/" + image_03_file);
                    www04 = new WWW(urlGetfromServer + "pictures/" + image_04_file);
                    www05 = new WWW(urlGetfromServer + "pictures/" + image_05_file); // niki
                    www06 = new WWW(urlGetfromServer + "pictures/" + image_06_file); // boxer
                }
                if (resources_files == "local_file_source" || resources_files == "streaming_source")
                {
                    ArtworkThumbnail1.GetComponent<RawImage>().texture = www01.texture;
                    ArtworkThumbnail2.GetComponent<RawImage>().texture = www02.texture;
                    ArtworkThumbnail3.GetComponent<RawImage>().texture = www03.texture;
                    ArtworkThumbnail4.GetComponent<RawImage>().texture = www04.texture;
                    introImages = new Texture[4] { www01.texture, www02.texture, www03.texture, www04.texture };

                    ArtworkThumbnail1.GetComponent<Button>().onClick.AddListener(delegate { ArtworkImageShow(www01.texture, 1); });
                    ArtworkThumbnail2.GetComponent<Button>().onClick.AddListener(delegate { ArtworkImageShow(www02.texture, 2); });
                    ArtworkThumbnail3.GetComponent<Button>().onClick.AddListener(delegate { ArtworkImageShow(www03.texture, 3); });
                    ArtworkThumbnail4.GetComponent<Button>().onClick.AddListener(delegate { ArtworkImageShow(www04.texture, 4); });
                }
            }

        }

        // Use this for initialization
        void Start()
        {

            if (resources_files == "local_file_source")
            {
#if UNITY_EDITOR
                if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(urlsave)))
                {
#elif UNITY_ANDROID || UNITY_IOS 
                    
                /*    DownloadPanelText.GetComponent<Text>().text = DownloadPanelText.GetComponent<Text>().text + "\n\n01" + System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(urlsave)) + "-" + System.IO.Path.GetDirectoryName(urlsave);
                    DownloadPanelText.GetComponent<Text>().text = DownloadPanelText.GetComponent<Text>().text + "\n\n02" + System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(System.IO.Path.Combine(GetDownloadFolder() , urlsaveAndroid))) + "-" + System.IO.Path.GetDirectoryName(System.IO.Path.Combine(GetDownloadFolder() , urlsaveAndroid)) ;
                    DownloadPanelText.GetComponent<Text>().text = DownloadPanelText.GetComponent<Text>().text + "\n\n03" + System.IO.Directory.Exists(System.IO.Path.Combine("file://" + GetDownloadFolder(), urlsaveAndroid)) + "-" + System.IO.Path.Combine("file://" + GetDownloadFolder(), urlsaveAndroid);
                    DownloadPanelText.GetComponent<Text>().text = DownloadPanelText.GetComponent<Text>().text + "\n\n04" + System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(System.IO.Path.Combine("file://" + GetDownloadFolder() , urlsaveAndroid))) + "-" + System.IO.Path.GetDirectoryName(System.IO.Path.Combine("file://" + GetDownloadFolder() , urlsaveAndroid));
                    DownloadPanelText.GetComponent<Text>().text = DownloadPanelText.GetComponent<Text>().text + "\n\n05" + System.IO.Directory.Exists(System.IO.Path.GetDirectoryName("file://" + GetDownloadFolder() + urlsaveAndroid)) + "-" + System.IO.Path.GetDirectoryName("file://" + GetDownloadFolder() + urlsaveAndroid);
                    DownloadPanelText.GetComponent<Text>().text = DownloadPanelText.GetComponent<Text>().text + "\n\n06" + System.IO.Directory.Exists(System.IO.Path.GetDirectoryName( GetDownloadFolder() + urlsaveAndroid)) + "-" + System.IO.Path.GetDirectoryName(GetDownloadFolder() + urlsaveAndroid);*/


                   if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName( GetDownloadFolder() + urlsaveAndroid)))
                { 
#endif
                    showDownloadPanel();
                    WWW wwwServer01 = new WWW(urlGetfromServer + "video/" + video_1_file);
                    WWW wwwServer02 = new WWW(urlGetfromServer + "video/" + video_2_file);
                    WWW wwwServer03 = new WWW(urlGetfromServer + "audio/" + sound_1_file);
                    WWW wwwServer04 = new WWW(urlGetfromServer + "audio/" + sound_2_file);
                    WWW wwwServer05 = new WWW(urlGetfromServer + "pictures/" + image_01_file);
                    WWW wwwServer06 = new WWW(urlGetfromServer + "pictures/" + image_02_file);
                    WWW wwwServer07 = new WWW(urlGetfromServer + "pictures/" + image_03_file);
                    WWW wwwServer08 = new WWW(urlGetfromServer + "pictures/" + image_04_file);
                    WWW wwwServer09 = new WWW(urlGetfromServer + "pictures/" + image_05_file); // niki
                    WWW wwwServer10 = new WWW(urlGetfromServer + "pictures/" + image_06_file); // boxer

#if UNITY_EDITOR
                    StartCoroutine(_downloadResourceAndSave(wwwServer01, urlsave + "video\\" + video_1_file, true));
                    StartCoroutine(_downloadResourceAndSave(wwwServer02, urlsave + "video\\" + video_2_file, false));
                    StartCoroutine(_downloadResourceAndSave(wwwServer03, urlsave + "audio\\" + sound_1_file, false));
                    StartCoroutine(_downloadResourceAndSave(wwwServer04, urlsave + "audio\\" + sound_2_file, false));
                    StartCoroutine(_downloadResourceAndSave(wwwServer05, urlsave + "pictures\\" + image_01_file, false));
                    StartCoroutine(_downloadResourceAndSave(wwwServer06, urlsave + "pictures\\" + image_02_file, false));
                    StartCoroutine(_downloadResourceAndSave(wwwServer07, urlsave + "pictures\\" + image_03_file, false));
                    StartCoroutine(_downloadResourceAndSave(wwwServer08, urlsave + "pictures\\" + image_04_file, false));
                    StartCoroutine(_downloadResourceAndSave(wwwServer09, urlsave + "pictures\\" + image_05_file, false));// niki
                    StartCoroutine(_downloadResourceAndSave(wwwServer10, urlsave + "pictures\\" + image_06_file, false));// boxer

                    //  StartCoroutine(_downloadResourceAndSave(wwwServer, pathToSaveImage));
#elif UNITY_ANDROID || UNITY_IOS
                StartCoroutine(_downloadResourceAndSave(wwwServer01, System.IO.Path.Combine(GetDownloadFolder() + urlsaveAndroid + "video/", video_1_file) , true));
                StartCoroutine(_downloadResourceAndSave(wwwServer02, System.IO.Path.Combine(GetDownloadFolder() + urlsaveAndroid + "video/", video_2_file) , false));
                StartCoroutine(_downloadResourceAndSave(wwwServer03, System.IO.Path.Combine(GetDownloadFolder() + urlsaveAndroid + "audio/", sound_1_file) , false));
                StartCoroutine(_downloadResourceAndSave(wwwServer04, System.IO.Path.Combine(GetDownloadFolder() + urlsaveAndroid + "audio/", sound_2_file) , false));
                StartCoroutine(_downloadResourceAndSave(wwwServer05, System.IO.Path.Combine(GetDownloadFolder() + urlsaveAndroid + "pictures/", image_01_file) , false));
                StartCoroutine(_downloadResourceAndSave(wwwServer06, System.IO.Path.Combine(GetDownloadFolder() + urlsaveAndroid + "pictures/", image_02_file) , false));
                StartCoroutine(_downloadResourceAndSave(wwwServer07, System.IO.Path.Combine(GetDownloadFolder() + urlsaveAndroid + "pictures/", image_03_file) , false));
                StartCoroutine(_downloadResourceAndSave(wwwServer08, System.IO.Path.Combine(GetDownloadFolder() + urlsaveAndroid + "pictures/", image_04_file) , false));
                StartCoroutine(_downloadResourceAndSave(wwwServer09, System.IO.Path.Combine(GetDownloadFolder() + urlsaveAndroid + "pictures/", image_05_file) , false));
                StartCoroutine(_downloadResourceAndSave(wwwServer10, System.IO.Path.Combine(GetDownloadFolder() + urlsaveAndroid + "pictures/", image_06_file) , false));
#endif

                }
            }

            PanelInfo.gameObject.SetActive(false);
            PanelVideo.gameObject.SetActive(false);


            ManualPanelText.GetComponent<Text>().text = ManualPanelTextStringDE;
            CloseManualTextButton.GetComponent<Text>().text = CloseManualTextButtonDE;
            ThreeDbuttonCloseText.GetComponent<Text>().text = ThreeDbuttonCloseTextDE;


            imagePlayer.gameObject.SetActive(true);


            //Show and Close of Video Panel
            Videobutton.GetComponent<Button>().onClick.AddListener(delegate { showVideoPanel(current_video, current_video_url); });
            ButtonCloseVideoPanel.GetComponent<Button>().onClick.AddListener(delegate { hideVideoPanel(); });
            ButtonQuestion.GetComponent<Button>().onClick.AddListener(delegate { showManualPanel(); });
            CloseManualButton.GetComponent<Button>().onClick.AddListener(delegate { CloseManual(); });

            PlayButton.GetComponent<Button>().onClick.AddListener(delegate { PlayPauseVideo(); });
            PauseButton.GetComponent<Button>().onClick.AddListener(delegate { PlayPauseVideo(); });
            FullScreenVideoButton.GetComponent<Button>().onClick.AddListener(delegate { FullScreenVideo(); });

   
            ButtonCloseImagePanel.GetComponent<Button>().onClick.AddListener(delegate { hideImagePanel(); });

            //Show and Close of AudioThumbnailWindow
            //  ButtonShowAudioThumbnailWindow.GetComponent<Button>().onClick.AddListener(delegate { showAudioThumbnailWindow(); });
            Audiobutton.GetComponent<Button>().onClick.AddListener(delegate { showAudioThumbnailWindow(); });
            Textbutton.GetComponent<Button>().onClick.AddListener(delegate { showTextThumbnailWindow(); });
            Gallerybutton.GetComponent<Button>().onClick.AddListener(delegate { showGalleryThumbnailWindow(); });
            CloseButtonGallery.GetComponent<Button>().onClick.AddListener(delegate { CloseGalleryThumbnailWindow(); });

            ThreeDbutton.GetComponent<Button>().onClick.AddListener(delegate { OpenThreeDPanel(); });
            ThreeDbuttonClose.GetComponent<Button>().onClick.AddListener(delegate { CloseThreeDPanel(); });

            LeftButtonImage.GetComponent<Button>().onClick.AddListener(delegate { LeftButtonImageGoFunction(); });
            RightButtonImage.GetComponent<Button>().onClick.AddListener(delegate { RightButtonImageGoFunction(); });


            if (resources_files == "unity_source")  //local_file_source: streaming_source: unity_source
            {//unity_source
                ArtworkThumbnail1.GetComponent<RawImage>().texture = image_01;
                ArtworkThumbnail2.GetComponent<RawImage>().texture = image_02;
                ArtworkThumbnail3.GetComponent<RawImage>().texture = image_03;
                ArtworkThumbnail4.GetComponent<RawImage>().texture = image_04;
                introImages = new Texture[4] { image_01, image_02, image_03, image_04 };
                ArtworkThumbnail1.GetComponent<Button>().onClick.AddListener(delegate { ArtworkImageShow(image_01, 1); });
                ArtworkThumbnail2.GetComponent<Button>().onClick.AddListener(delegate { ArtworkImageShow(image_02, 2); });
                ArtworkThumbnail3.GetComponent<Button>().onClick.AddListener(delegate { ArtworkImageShow(image_03, 3); });
                ArtworkThumbnail4.GetComponent<Button>().onClick.AddListener(delegate { ArtworkImageShow(image_04, 4); });
            }
            else if (resources_files == "local_file_source")
            { //local_file_source
#if UNITY_EDITOR
                www01 = new WWW(urlsave + "pictures\\" + image_01_file);
                www02 = new WWW(urlsave + "pictures\\" + image_02_file);
                www03 = new WWW(urlsave + "pictures\\" + image_03_file);
                www04 = new WWW(urlsave + "pictures\\" + image_04_file);
                www05 = new WWW(urlsave + "pictures\\" + image_05_file); // niki
                www06 = new WWW(urlsave + "pictures\\" + image_06_file); // boxer

#elif UNITY_ANDROID || UNITY_IOS 
                 www01 = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "pictures/" + image_01_file);
                 www02 = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "pictures/" + image_02_file);
                 www03 = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "pictures/" + image_03_file);
                 www04 = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "pictures/" + image_04_file);
                 www05 = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "pictures/" + image_05_file); // niki
                 www06 = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "pictures/" + image_06_file); // boxer
#endif
            }
            else
            {//streaming_source
                www01 = new WWW(urlGetfromServer + "pictures/" + image_01_file);
                www02 = new WWW(urlGetfromServer + "pictures/" + image_02_file);
                www03 = new WWW(urlGetfromServer + "pictures/" + image_03_file);
                www04 = new WWW(urlGetfromServer + "pictures/" + image_04_file);
                www05 = new WWW(urlGetfromServer + "pictures/" + image_05_file); // niki
                www06 = new WWW(urlGetfromServer + "pictures/" + image_06_file); // boxer
            }
            if (resources_files == "local_file_source" || resources_files == "streaming_source")
            {
                ArtworkThumbnail1.GetComponent<RawImage>().texture = www01.texture;
                ArtworkThumbnail2.GetComponent<RawImage>().texture = www02.texture;
                ArtworkThumbnail3.GetComponent<RawImage>().texture = www03.texture;
                ArtworkThumbnail4.GetComponent<RawImage>().texture = www04.texture;
                introImages = new Texture[4] { www01.texture, www02.texture, www03.texture, www04.texture };

                ArtworkThumbnail1.GetComponent<Button>().onClick.AddListener(delegate { ArtworkImageShow(www01.texture, 1); });
                ArtworkThumbnail2.GetComponent<Button>().onClick.AddListener(delegate { ArtworkImageShow(www02.texture, 2); });
                ArtworkThumbnail3.GetComponent<Button>().onClick.AddListener(delegate { ArtworkImageShow(www03.texture, 3); });
                ArtworkThumbnail4.GetComponent<Button>().onClick.AddListener(delegate { ArtworkImageShow(www04.texture, 4); });
            }



            ButtonPlayStopSound.GetComponent<Button>().onClick.AddListener(delegate { StartCoroutine(playSound(current_sound, sound_plays)); });
            ButtonEnglish.GetComponent<Button>().onClick.AddListener(delegate { ChangeLanguage(); });
            ButtonGerman.GetComponent<Button>().onClick.AddListener(delegate { ChangeLanguage(); });


        }

        // Update is called once per frame
        void Update()
        {
            StateManager sm = TrackerManager.Instance.GetStateManager();
            IEnumerable<TrackableBehaviour> tbs = sm.GetActiveTrackableBehaviours();
            if (video_plays)
            {
                if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft || Input.deviceOrientation == DeviceOrientation.LandscapeRight)
                {
                   // Debug.Log("we landscape now.");
                    if (current_FullScreenVideo == 0) { FullScreenVideo(); }

                }
                else if (Input.deviceOrientation == DeviceOrientation.Portrait)
                {
                   // Debug.Log("we portrait now");
                    if (current_FullScreenVideo == 1) { FullScreenVideo(); }

                }
            }



            if (sound_plays)
            {
                slider.value = soundTarget.time / soundTarget.clip.length;
            }

            if (Search_Target)
            {
                if (is_ThreeDPanel_close)
                {
                    foreach (TrackableBehaviour tb in tbs)
                    {

                        string name = tb.TrackableName;



                        if (name == "niki" & Search_Target_active != "niki")
                        {
                            ModalImage.gameObject.SetActive(true);
                            Search_Target_active = "niki";
                            Search_Target = true;
                            AudioThumbnailWindow.gameObject.SetActive(false);
                            ButtonCloseImagePanel.GetComponent<Button>().onClick.AddListener(delegate { hideImagePanel(); });
                            if (resources_files == "unity_source")  //local_file_source: streaming_source: unity_source
                            {//unity_source
                                PreviewImage.GetComponent<RawImage>().texture = image_05;
                            }
                            else { PreviewImage.GetComponent<RawImage>().texture = www05.texture; }


                            VideobuttonGrey.gameObject.SetActive(false);
                            Videobutton.gameObject.SetActive(true);
                            AudiobuttonGrey.gameObject.SetActive(false);
                            Audiobutton.gameObject.SetActive(true);
                            TextbuttonGrey.gameObject.SetActive(false);
                            Textbutton.gameObject.SetActive(true);
                            GallerybuttonGrey.gameObject.SetActive(true);
                            Gallerybutton.gameObject.SetActive(false);
                            ThreeDbuttonGrey.gameObject.SetActive(false);
                            ThreeDbutton.gameObject.SetActive(true);

                            current_video = video_1;
                            current_video_url = video_2_file;
                            if (resources_files == "unity_source")
                            {
                                current_sound = sound_1_unity;
                            }
                            else { current_sound = sound_1_file; }

                            current_TrackableName = 0; // niki ==> 0
                            if (current_Language == "De")
                            {
                                TitleOfVideo.GetComponent<Text>().text = TitleOfVideoStringDE[current_TrackableName];
                                VideoDescriptionLong.GetComponent<Text>().text = VideoDescriptionLongStringDE[current_TrackableName];
                                TextDescription.GetComponent<Text>().text = TextDescriptionStringDE[current_TrackableName];
                                AudioTextDescription.GetComponent<Text>().text = AudioTextDescriptionStringDE[current_TrackableName];
                            }
                            else
                            {
                                TitleOfVideo.GetComponent<Text>().text = TitleOfVideoStringEN[current_TrackableName];
                                VideoDescriptionLong.GetComponent<Text>().text = VideoDescriptionLongStringEN[current_TrackableName];
                                TextDescription.GetComponent<Text>().text = TextDescriptionStringEN[current_TrackableName];
                                AudioTextDescription.GetComponent<Text>().text = AudioTextDescriptionStringEN[current_TrackableName];
                            }

                        }





                        if (name == "hiroshige_wellen1061" & Search_Target_active != "hiroshige_wellen1061")
                        {
                            ModalImage.gameObject.SetActive(true);
                            Search_Target = true;
                            Search_Target_active = "hiroshige_wellen1061";
                            AudioThumbnailWindow.gameObject.SetActive(false);
                            if (resources_files == "unity_source")  //local_file_source: streaming_source: unity_source
                            {//unity_source
                                PreviewImage.GetComponent<RawImage>().texture = image_04;
                            }
                            else { PreviewImage.GetComponent<RawImage>().texture = www04.texture; }


                            VideobuttonGrey.gameObject.SetActive(false);
                            Videobutton.gameObject.SetActive(true);
                            AudiobuttonGrey.gameObject.SetActive(false);
                            Audiobutton.gameObject.SetActive(true);
                            TextbuttonGrey.gameObject.SetActive(false);
                            Textbutton.gameObject.SetActive(true);
                            GallerybuttonGrey.gameObject.SetActive(false);
                            Gallerybutton.gameObject.SetActive(true);
                            ThreeDbuttonGrey.gameObject.SetActive(true);
                            ThreeDbutton.gameObject.SetActive(false);

                            current_video = video_2;
                            current_video_url = video_1_file;
                            if (resources_files == "unity_source")
                            {
                                current_sound = sound_2_unity;
                            }
                            else { current_sound = sound_2_file; }

                            current_TrackableName = 1; // for hiroshige_wellen1061 ==>1
                            if (current_Language == "De")
                            {
                                TitleOfVideo.GetComponent<Text>().text = TitleOfVideoStringDE[current_TrackableName];
                                VideoDescriptionLong.GetComponent<Text>().text = VideoDescriptionLongStringDE[current_TrackableName];
                                TextDescription.GetComponent<Text>().text = TextDescriptionStringDE[current_TrackableName];
                                AudioTextDescription.GetComponent<Text>().text = AudioTextDescriptionStringDE[current_TrackableName];
                            }
                            else
                            {
                                TitleOfVideo.GetComponent<Text>().text = TitleOfVideoStringEN[current_TrackableName];
                                VideoDescriptionLong.GetComponent<Text>().text = VideoDescriptionLongStringEN[current_TrackableName];
                                TextDescription.GetComponent<Text>().text = TextDescriptionStringEN[current_TrackableName];
                                AudioTextDescription.GetComponent<Text>().text = AudioTextDescriptionStringEN[current_TrackableName];
                            }
                        }

                        if (name == "vuforia_stones_vergium_crop" & Search_Target_active != "vuforia_stones_vergium_crop")
                        {
                            ModalImage.gameObject.SetActive(true);
                            Search_Target = true;
                            Search_Target_active = "vuforia_stones_vergium_crop";
                            AudioThumbnailWindow.gameObject.SetActive(false);
                            ButtonCloseImagePanel.GetComponent<Button>().onClick.AddListener(delegate { hideImagePanel(); });
                            if (resources_files == "unity_source")  //local_file_source: streaming_source: unity_source
                            {//unity_source
                                PreviewImage.GetComponent<RawImage>().texture = image_06;
                            }
                            else { PreviewImage.GetComponent<RawImage>().texture = www06.texture; }

                            VideobuttonGrey.gameObject.SetActive(false);
                            Videobutton.gameObject.SetActive(true);
                            AudiobuttonGrey.gameObject.SetActive(false);
                            Audiobutton.gameObject.SetActive(true);
                            TextbuttonGrey.gameObject.SetActive(false);
                            Textbutton.gameObject.SetActive(true);
                            GallerybuttonGrey.gameObject.SetActive(true);
                            Gallerybutton.gameObject.SetActive(false);
                            ThreeDbuttonGrey.gameObject.SetActive(false);
                            ThreeDbutton.gameObject.SetActive(true);

                            current_video = video_3;
                            current_video_url = video_3_file;
                            if (resources_files == "unity_source")
                            {
                                current_sound = sound_3_unity;
                            }
                            else { current_sound = sound_3_file; }

                            current_TrackableName = 2; // boxer ==> 2
                            if (current_Language == "De")
                            {
                                TitleOfVideo.GetComponent<Text>().text = TitleOfVideoStringDE[current_TrackableName];
                                VideoDescriptionLong.GetComponent<Text>().text = VideoDescriptionLongStringDE[current_TrackableName];
                                TextDescription.GetComponent<Text>().text = TextDescriptionStringDE[current_TrackableName];
                                AudioTextDescription.GetComponent<Text>().text = AudioTextDescriptionStringDE[current_TrackableName];
                            }
                            else
                            {
                                TitleOfVideo.GetComponent<Text>().text = TitleOfVideoStringEN[current_TrackableName];
                                VideoDescriptionLong.GetComponent<Text>().text = VideoDescriptionLongStringEN[current_TrackableName];
                                TextDescription.GetComponent<Text>().text = TextDescriptionStringEN[current_TrackableName];
                                AudioTextDescription.GetComponent<Text>().text = AudioTextDescriptionStringEN[current_TrackableName];
                            }
                        }


                    }
                }
            }
        }



        void ChangeLanguage()
        {
            if (current_Language == "De")
            {
                ButtonEnglish.gameObject.SetActive(false);
                ButtonGerman.gameObject.SetActive(true);
                current_Language = "En";
                ArtworkText.GetComponent<Text>().text = galleryPictureStringEN[current_Picture_gallery - 1];
                GalleryTextDescription.GetComponent<Text>().text = galleryDescriptionStringEN;
                TitleOfVideo.GetComponent<Text>().text = TitleOfVideoStringEN[current_TrackableName];
                VideoDescriptionLong.GetComponent<Text>().text = VideoDescriptionLongStringEN[current_TrackableName];
                TextDescription.GetComponent<Text>().text = TextDescriptionStringEN[current_TrackableName];
                AudioTextDescription.GetComponent<Text>().text = AudioTextDescriptionStringEN[current_TrackableName];
                ManualPanelText.GetComponent<Text>().text = ManualPanelTextStringEN;
                CloseManualTextButton.GetComponent<Text>().text = CloseManualTextButtonEN;
                ThreeDbuttonCloseText.GetComponent<Text>().text = ThreeDbuttonCloseTextEN;
            }
            else
            {
                ButtonEnglish.gameObject.SetActive(true);
                ButtonGerman.gameObject.SetActive(false);
                current_Language = "De";
                ArtworkText.GetComponent<Text>().text = galleryPictureStringDE[current_Picture_gallery - 1];
                GalleryTextDescription.GetComponent<Text>().text = galleryDescriptionStringDE;
                TitleOfVideo.GetComponent<Text>().text = TitleOfVideoStringDE[current_TrackableName];
                VideoDescriptionLong.GetComponent<Text>().text = VideoDescriptionLongStringDE[current_TrackableName];
                TextDescription.GetComponent<Text>().text = TextDescriptionStringDE[current_TrackableName];
                AudioTextDescription.GetComponent<Text>().text = AudioTextDescriptionStringDE[current_TrackableName];
                ManualPanelText.GetComponent<Text>().text = ManualPanelTextStringDE;
                CloseManualTextButton.GetComponent<Text>().text = CloseManualTextButtonDE;
                ThreeDbuttonCloseText.GetComponent<Text>().text = ThreeDbuttonCloseTextDE;
            }

        }




        void showVideoPanel(VideoClip current_video, string current_video_url)
        {

            if (!show_VideoPanel)
            {
                cam.enabled = false;
                VideoThumbnailWindow.gameObject.SetActive(true);
                GalleryDescriptionPanel.gameObject.SetActive(false);
                GalleryPanel.gameObject.SetActive(false);
                TextPanel.gameObject.SetActive(false);
                AudioThumbnailWindow.gameObject.SetActive(false);
                PanelVideo.gameObject.SetActive(true);
                video_plays = true;
                show_VideoPanel = true;

                //  videoPlayer.videoUrl = "https://gogu.gotresor.de/VideoStream/CeramicSpeed%2099-%20Efficient%20Drive%20Shaft%20--%20Chain%20Free%20Bike%20--%20Eurobike%202018.mp4";

                if (resources_files == "unity_source")  //local_file_source: streaming_source: unity_source
                {//unity_source
                    videoPlayer.StartVideo(current_video, false);
                }
                else if (resources_files == "local_file_source")
                { //local_file_source
                    videoPlayer.isyoutube = true;
#if UNITY_EDITOR
                    videoPlayer.videoUrl = urlsave + "video\\" + current_video_url;
#elif UNITY_ANDROID || UNITY_IOS
                                          videoPlayer.videoUrl = "file://" + GetDownloadFolder() + urlsaveAndroid +  "video/" + current_video_url;
#endif
                    videoPlayer.StartVideo(null, is_youtube);
                    Debug.Log(videoPlayer.unityVideoPlayerHeight + " - " + videoPlayer.unityVideoPlayerWidth);
                }
                else
                {//streaming_source
                    videoPlayer.isyoutube = true;
                    videoPlayer.videoUrl = urlGetfromServer + "video/" + current_video_url;
                    videoPlayer.StartVideo(null, is_youtube);
                }
            }
            else
            {
                hideVideoPanel();
            }

            //  Handheld.PlayFullScreenMovie(current_video, Color.black, FullScreenMovieControlMode.CancelOnInput);

            if (sound_plays)
            {

                sound_plays = false;

                ButtonPlayStopSound.GetComponent<UnityEngine.UI.Image>().sprite = butt_play_sprite;

                soundTarget.Stop();

            }


        }

        void FullScreenVideo()
        {
            if (current_FullScreenVideo == 0)
            {
                BottomPanel.gameObject.SetActive(false);
                //   Screen.orientation = ScreenOrientation.Landscape;
                PanelVideo.GetComponent<RectTransform>().Rotate(new Vector3(0, 0, -90));
                RectTransform rectTransform = PanelVideo.GetComponent<RectTransform>();
                RectTransform parentTransform = PanelVideo.parent.GetComponent<RectTransform>();
                float aspectRatio = parentTransform.rect.size.x / parentTransform.rect.size.y;
                float halfAspectRatio = aspectRatio / 2.0f;
                float halfAspectRatioInvert = (1.0f / aspectRatio) / 2.0f;
                rectTransform.anchorMin = new Vector2(0.5f - halfAspectRatioInvert, 0.5f - halfAspectRatio);
                rectTransform.anchorMax = new Vector2(0.5f + halfAspectRatioInvert, 0.5f + halfAspectRatio);
                rectTransform.anchoredPosition = Vector3.zero;
                rectTransform.offsetMin = Vector2.zero;
                rectTransform.offsetMax = Vector2.zero;

                //   PanelVideo.GetComponent<RectTransform>().offsetMin = new Vector2(left, bottom);
                //   PanelVideo.GetComponent<RectTransform>().offsetMax = new Vector2(-right, -top);
                //  PanelVideo.GetComponent<RectTransform>().offsetMin = new Vector2(-391, 400);
                //   PanelVideo.GetComponent<RectTransform>().offsetMax = new Vector2(400, -400);


                BackVideoPanel.GetComponent<RectTransform>().offsetMin = new Vector2(0, 0);
                BackVideoPanel.GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);
                //++  VideoPlayerRaw.GetComponent<RectTransform>().offsetMin = new Vector2(-1, 5);

                /*  Progress.GetComponent<RectTransform>().offsetMin = new Vector2(76, 20);
                Progress.GetComponent<RectTransform>().offsetMax = new Vector2(-90, -980);
                BackgroundProgres.GetComponent<RectTransform>().offsetMin = new Vector2(76, 20);
                BackgroundProgres.GetComponent<RectTransform>().offsetMax = new Vector2(-90, -980);
                PlayButton.GetComponent<RectTransform>().offsetMin = new Vector2(-1, 5);
                PlayButton.GetComponent<RectTransform>().offsetMax = new Vector2(-1739, -952);
                PauseButton.GetComponent<RectTransform>().offsetMin = new Vector2(-1, 5);
                PauseButton.GetComponent<RectTransform>().offsetMax = new Vector2(-1739, -952);
                  FullScreenVideoButton.GetComponent<RectTransform>().offsetMin = new Vector2(2000, 2);
                  FullScreenVideoButton.GetComponent<RectTransform>().offsetMax = new Vector2(-20, -952);*/
                /*  Progress.gameObject.SetActive(false);
                  BackgroundProgres.gameObject.SetActive(false);
                  PlayButton.gameObject.SetActive(false);
                  PauseButton.gameObject.SetActive(false);
                  FullScreenVideoButton.gameObject.SetActive(false);

                  ProgressFULL.gameObject.SetActive(true);
                  BackgroundProgresFULL.gameObject.SetActive(true);
                  FullScreenVideoButtonFULL.gameObject.SetActive(true);
                  if (video_plays)
                  {
                      PlayButtonFULL.gameObject.SetActive(false);
                      PauseButtonFULL.gameObject.SetActive(true);
                  }
                  else
                  {
                      PlayButtonFULL.gameObject.SetActive(true);
                      PauseButtonFULL.gameObject.SetActive(false);
                  }*/

                //TitleOfVideo.GetComponent<RectTransform>().offsetMax = new Vector2(-462, -860);
                TitleOfVideo.gameObject.SetActive(false);
                current_FullScreenVideo = 1;
            }
            else
            {
                BottomPanel.gameObject.SetActive(true);
                //     Screen.orientation = ScreenOrientation.Portrait;
                PanelVideo.GetComponent<RectTransform>().Rotate(new Vector3(0, 0, 90));
                RectTransform rectTransform = PanelVideo.GetComponent<RectTransform>();
                //  RectTransform parentTransform = ModalImage.GetComponent<RectTransform>();
                // float aspectRatio = parentTransform.rect.size.x / parentTransform.rect.size.y;
                //      float halfAspectRatio = aspectRatio / 2.0f;
                //      float halfAspectRatioInvert = (1.0f / aspectRatio) / 2.0f;
                rectTransform.anchorMin = new Vector2(0, 0);
                rectTransform.anchorMax = new Vector2(1, 1);
                rectTransform.anchoredPosition = Vector3.zero;

                BackVideoPanel.GetComponent<RectTransform>().offsetMin = new Vector2(0, 209);
                BackVideoPanel.GetComponent<RectTransform>().offsetMax = new Vector2(0, -66);

                PanelVideo.GetComponent<RectTransform>().offsetMin = new Vector2(0, 882);
                PanelVideo.GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);
                BackVideoPanel.GetComponent<RectTransform>().offsetMin = new Vector2(0, 209);
                BackVideoPanel.GetComponent<RectTransform>().offsetMax = new Vector2(0, -66);
                //++     VideoPlayerRaw.GetComponent<RectTransform>().offsetMin = new Vector2(11, 240);

                /*   Progress.GetComponent<RectTransform>().offsetMin = new Vector2(76, 260);
                Progress.GetComponent<RectTransform>().offsetMax = new Vector2(-90, -616);
                BackgroundProgres.GetComponent<RectTransform>().offsetMin = new Vector2(76, 260);
                BackgroundProgres.GetComponent<RectTransform>().offsetMax = new Vector2(-90, -616);

                PlayButton.GetComponent<RectTransform>().offsetMin = new Vector2(19, 241);
                PlayButton.GetComponent<RectTransform>().offsetMax = new Vector2(-955, -605);
                PauseButton.GetComponent<RectTransform>().offsetMin = new Vector2(19, 241);
                  PauseButton.GetComponent<RectTransform>().offsetMax = new Vector2(-955, -605);*/

                //   FullScreenVideoButton.GetComponent<RectTransform>().offsetMin = new Vector2(954, 250);
                //   FullScreenVideoButton.GetComponent<RectTransform>().offsetMax = new Vector2(-20, -927);
                //TitleOfVideo.GetComponent<RectTransform>().offsetMax = new Vector2(-462, -702);
                /*  Progress.gameObject.SetActive(true);
                  BackgroundProgres.gameObject.SetActive(true);
                  FullScreenVideoButton.gameObject.SetActive(true);

                  ProgressFULL.gameObject.SetActive(false);
                  BackgroundProgresFULL.gameObject.SetActive(false);
                  FullScreenVideoButtonFULL.gameObject.SetActive(false);
                  PlayButtonFULL.gameObject.SetActive(false);
                  PauseButtonFULL.gameObject.SetActive(false);

                  if (video_plays)
                  {
                      PlayButton.gameObject.SetActive(false);
                      PauseButton.gameObject.SetActive(true);
                  }
                  else
                  {
                      PlayButton.gameObject.SetActive(true);
                      PauseButton.gameObject.SetActive(false);
                  }*/
                TitleOfVideo.gameObject.SetActive(true);
                current_FullScreenVideo = 0;
            }


        }
        void PlayPauseVideo()
        {
            if (video_plays)
            {
                PlayButton.gameObject.SetActive(true);
                PauseButton.gameObject.SetActive(false);
                video_plays = false;
                videoPlayer.PlayStopVideo();
            }
            else
            {
                PlayButton.gameObject.SetActive(false);
                PauseButton.gameObject.SetActive(true);
                video_plays = true;
                videoPlayer.PlayStopVideo();
            }


        }


        void hideVideoPanel()
        {
            //   cam.enabled = true;
            PanelVideo.gameObject.SetActive(false);
            if (current_FullScreenVideo == 1)
            {
                PanelVideo.GetComponent<RectTransform>().Rotate(new Vector3(0, 0, 90));
                RectTransform rectTransform = PanelVideo.GetComponent<RectTransform>();
                //  RectTransform parentTransform = ModalImage.GetComponent<RectTransform>();
                // float aspectRatio = parentTransform.rect.size.x / parentTransform.rect.size.y;
                //      float halfAspectRatio = aspectRatio / 2.0f;
                //      float halfAspectRatioInvert = (1.0f / aspectRatio) / 2.0f;
                rectTransform.anchorMin = new Vector2(0, 0);
                rectTransform.anchorMax = new Vector2(1, 1);
                rectTransform.anchoredPosition = Vector3.zero;

                BackVideoPanel.GetComponent<RectTransform>().offsetMin = new Vector2(0, 209);
                BackVideoPanel.GetComponent<RectTransform>().offsetMax = new Vector2(0, -66);

                PanelVideo.GetComponent<RectTransform>().offsetMin = new Vector2(0, 882);
                PanelVideo.GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);
                BackVideoPanel.GetComponent<RectTransform>().offsetMin = new Vector2(0, 209);
                BackVideoPanel.GetComponent<RectTransform>().offsetMax = new Vector2(0, -66);
                //++     VideoPlayerRaw.GetComponent<RectTransform>().offsetMin = new Vector2(11, 240);

                /*   Progress.GetComponent<RectTransform>().offsetMin = new Vector2(76, 260);
                Progress.GetComponent<RectTransform>().offsetMax = new Vector2(-90, -616);
                BackgroundProgres.GetComponent<RectTransform>().offsetMin = new Vector2(76, 260);
                BackgroundProgres.GetComponent<RectTransform>().offsetMax = new Vector2(-90, -616);

                PlayButton.GetComponent<RectTransform>().offsetMin = new Vector2(19, 241);
                PlayButton.GetComponent<RectTransform>().offsetMax = new Vector2(-955, -605);
                PauseButton.GetComponent<RectTransform>().offsetMin = new Vector2(19, 241);
                  PauseButton.GetComponent<RectTransform>().offsetMax = new Vector2(-955, -605);*/

                //   FullScreenVideoButton.GetComponent<RectTransform>().offsetMin = new Vector2(954, 250);
                //   FullScreenVideoButton.GetComponent<RectTransform>().offsetMax = new Vector2(-20, -927);
                //TitleOfVideo.GetComponent<RectTransform>().offsetMax = new Vector2(-462, -702);
                /*  Progress.gameObject.SetActive(true);
                  BackgroundProgres.gameObject.SetActive(true);
                  FullScreenVideoButton.gameObject.SetActive(true);

                  ProgressFULL.gameObject.SetActive(false);
                  BackgroundProgresFULL.gameObject.SetActive(false);
                  FullScreenVideoButtonFULL.gameObject.SetActive(false);
                  PlayButtonFULL.gameObject.SetActive(false);
                  PauseButtonFULL.gameObject.SetActive(false);

                  if (video_plays)
                  {
                      PlayButton.gameObject.SetActive(false);
                      PauseButton.gameObject.SetActive(true);
                  }
                  else
                  {
                      PlayButton.gameObject.SetActive(true);
                      PauseButton.gameObject.SetActive(false);
                  }*/
                TitleOfVideo.gameObject.SetActive(true);
            }
            VideoThumbnailWindow.gameObject.SetActive(false);
            videoPlayer.Stop();
            video_plays = false;
            show_VideoPanel = false;
            BottomPanel.gameObject.SetActive(true);
            current_FullScreenVideo = 0;

        }



        void hideImagePanel()
        {
            cam.enabled = true;
            ModalImage.gameObject.SetActive(false);

            //Buttons default --> Grey
            VideobuttonGrey.gameObject.SetActive(true);
            Videobutton.gameObject.SetActive(false);
            AudiobuttonGrey.gameObject.SetActive(true);
            Audiobutton.gameObject.SetActive(false);
            TextbuttonGrey.gameObject.SetActive(true);
            Textbutton.gameObject.SetActive(false);
            GallerybuttonGrey.gameObject.SetActive(true);
            Gallerybutton.gameObject.SetActive(false);
            ThreeDbuttonGrey.gameObject.SetActive(true);
            ThreeDbutton.gameObject.SetActive(false);

            GalleryDescriptionPanel.gameObject.SetActive(false);
            GalleryPanel.gameObject.SetActive(false);
            TextPanel.gameObject.SetActive(false);
            AudioThumbnailWindow.gameObject.SetActive(false);
            VideoThumbnailWindow.gameObject.SetActive(false);
            if (sound_plays == true)
            {
                playSound(current_sound, sound_plays);
            }
            Search_Target = true;
            Search_Target_active = "";
        }

        void OpenThreeDPanel()
        {
            cam.enabled = true;
            is_ThreeDPanel_close = false;
            VideoThumbnailWindow.gameObject.SetActive(false);
            PanelVideo.gameObject.SetActive(false);

            GalleryDescriptionPanel.gameObject.SetActive(false);
            GalleryPanel.gameObject.SetActive(false);
            TextPanel.gameObject.SetActive(false);
            AudioThumbnailWindow.gameObject.SetActive(false);

            ModalImage.gameObject.SetActive(false);

            BottomPanel.gameObject.SetActive(false);
            ThreeDPanel.gameObject.SetActive(true);


        }


        void CloseThreeDPanel()
        {
            cam.enabled = true;
            is_ThreeDPanel_close = true;
            ModalImage.gameObject.SetActive(true);

            BottomPanel.gameObject.SetActive(true);
            ThreeDPanel.gameObject.SetActive(false);


        }
        //new: Show and Close of AudioThumbnailWindow
        void showAudioThumbnailWindow()
        {
            cam.enabled = false;
            AudioThumbnailWindow.gameObject.SetActive(!AudioThumbnailWindow.gameObject.activeSelf);
            if (sound_plays == true)
            {
                playSound(current_sound, sound_plays);
            }
            TextPanel.gameObject.SetActive(false);
            GalleryDescriptionPanel.gameObject.SetActive(false);
            GalleryPanel.gameObject.SetActive(false);
            VideoThumbnailWindow.gameObject.SetActive(false);
            PanelVideo.gameObject.SetActive(false);
        }

        void showTextThumbnailWindow()
        {
            cam.enabled = false;
            TextPanel.gameObject.SetActive(!TextPanel.gameObject.activeSelf);
            GalleryDescriptionPanel.gameObject.SetActive(false);
            GalleryPanel.gameObject.SetActive(false);
            VideoThumbnailWindow.gameObject.SetActive(false);
            PanelVideo.gameObject.SetActive(false);
            AudioThumbnailWindow.gameObject.SetActive(false);

        }
        void showGalleryThumbnailWindow()
        {
            cam.enabled = false;
            GalleryDescriptionPanel.gameObject.SetActive(!GalleryDescriptionPanel.gameObject.activeSelf);
            if (resources_files == "unity_source")  //local_file_source: streaming_source: unity_source
            {//unity_source
                ArtworkImageShow(image_01, 1);
            }
            else
            { // "local_file_source" OR  "streaming_source"
                ArtworkImageShow(www01.texture, 1);
            }
            if (current_Language == "De")
            {
                ArtworkText.GetComponent<Text>().text = galleryPictureStringDE[0];
                GalleryTextDescription.GetComponent<Text>().text = galleryDescriptionStringDE;
            }
            else
            {
                ArtworkText.GetComponent<Text>().text = galleryPictureStringEN[0];
                GalleryTextDescription.GetComponent<Text>().text = galleryDescriptionStringEN;
            }


            GalleryPanel.gameObject.SetActive(!GalleryPanel.gameObject.activeSelf);
            // GalleryPanel.gameObject.SetActive(true);
            TextPanel.gameObject.SetActive(false);
            VideoThumbnailWindow.gameObject.SetActive(false);
            PanelVideo.gameObject.SetActive(false);
            AudioThumbnailWindow.gameObject.SetActive(false);

        }
        void CloseGalleryThumbnailWindow()
        {
            GalleryDescriptionPanel.gameObject.SetActive(false);
            GalleryPanel.gameObject.SetActive(false);
        }

        void ArtworkImageShow(Texture ArtworkImagePic, int _current_Picture_gallery)
        {

            ArtworkImage.GetComponent<RawImage>().texture = ArtworkImagePic;
            if (current_Language == "De")
            { ArtworkText.GetComponent<Text>().text = galleryPictureStringDE[_current_Picture_gallery - 1]; }
            else { ArtworkText.GetComponent<Text>().text = galleryPictureStringEN[_current_Picture_gallery - 1]; }

            current_Picture_gallery = _current_Picture_gallery;
        }

        void LeftButtonImageGoFunction()
        {
            int _temp = 0;
            _temp = current_Picture_gallery - 1;

            if (_temp > 0)
            {
                ArtworkImage.GetComponent<RawImage>().texture = introImages[_temp - 1];
                if (current_Language == "De")
                { ArtworkText.GetComponent<Text>().text = galleryPictureStringDE[_temp - 1]; }
                else { ArtworkText.GetComponent<Text>().text = galleryPictureStringEN[_temp - 1]; }
                current_Picture_gallery = _temp;
            }



        }
        void RightButtonImageGoFunction()
        {
            int _temp = 0;
            _temp = current_Picture_gallery + 1;

            if (_temp < 5)
            {
                ArtworkImage.GetComponent<RawImage>().texture = introImages[_temp - 1];
                if (current_Language == "De")
                { ArtworkText.GetComponent<Text>().text = galleryPictureStringDE[_temp - 1]; }
                else { ArtworkText.GetComponent<Text>().text = galleryPictureStringEN[_temp - 1]; }
                current_Picture_gallery = _temp;
            }
        }

        //function to play sound
        IEnumerator playSound(string ss, bool play)
        {
            Sprite current_sprite;

            if (!sound_plays)
            {

                if (resources_files == "unity_source")  //local_file_source: streaming_source: unity_source
                {//unity_source
                    clipTarget = (AudioClip)Resources.Load(ss);
                }
                else if (resources_files == "local_file_source")
                { //local_file_source
#if UNITY_EDITOR
                    WWW www = new WWW(urlsave + "audio\\" + ss);
                    yield return www;
#elif UNITY_ANDROID || UNITY_IOS 
                       WWW www = new WWW("file://" + GetDownloadFolder() + urlsaveAndroid +  "audio/" + ss);
                        yield return www;
#endif
                    clipTarget = www.GetAudioClip(false);
                    while (clipTarget.loadState != AudioDataLoadState.Loaded)
                        yield return new WaitForSeconds(0.1f);
                }
                else
                {//streaming_source
                    WWW www = new WWW(urlGetfromServer + "audio/" + ss);
                    yield return www;
                    clipTarget = www.GetAudioClip(false);
                    while (clipTarget.loadState != AudioDataLoadState.Loaded)
                        yield return new WaitForSeconds(0.1f);
                }

                soundTarget.clip = clipTarget;
                soundTarget.loop = false;
                soundTarget.playOnAwake = false;
                soundTarget.Play();
                sound_plays = true;

                current_sprite = butt_stop_sprite;
            }

            else
            {
                soundTarget.Pause();
                sound_plays = false;

                current_sprite = butt_play_sprite;

                if (ss == "")
                {
                    soundTarget.Stop();
                }

            }
            if (AudioThumbnailWindow != false)
            {
                PanelVideo.gameObject.SetActive(false);

            }

            ButtonPlayStopSound.GetComponent<UnityEngine.UI.Image>().sprite = current_sprite;

        }

        public void ChangeAudioTime()
        {
            soundTarget.time = soundTarget.clip.length * slider.value;
        }


        void deacvtivateBottomPanelButtons(bool yes_no)
        {
            Videobutton.GetComponent<Button>().interactable = yes_no;
            ButtonShowAudioThumbnailWindow.GetComponent<Button>().interactable = yes_no;
        }

        public static string GetDownloadFolder()
        {
            string[] temp = (Application.persistentDataPath.Replace("Android", "")).Split(new string[] { "//" }, System.StringSplitOptions.None);

            return (temp[0]);
        }
        private IEnumerator _downloadImage(WWW www)
        {
            yield return www;

            imagePlayer.GetComponent<RawImage>().texture = www.texture;

            //Check if we failed to send
            if (string.IsNullOrEmpty(www.error))
            {
                //  Debug.Log("Success");
            }
            else
            {
                //   Debug.Log("Error: " + www.error);
            }
        }



        private IEnumerator _downloadImageAndSave(WWW www, string savePath)
        {
            yield return www;

            //Check if we failed to send
            if (string.IsNullOrEmpty(www.error))
            {
                //  Debug.Log("Success");

                //Save Image
                saveImage(savePath, www.bytes);
            }
            else
            {
                //  Debug.Log("Error: " + www.error);
            }
        }


        void saveImage(string path, byte[] imageBytes)
        {

            //Create Directory if it does not exist
            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(path)))
            {
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
            }

            try
            {
                System.IO.File.WriteAllBytes(path, imageBytes);
                // Debug.Log("Saved Data to: " + path.Replace("/", "\\"));
            }
            catch (System.Exception e)
            {
                //  Debug.LogWarning("Failed To Save Data to: " + path.Replace("/", "\\"));
                //  Debug.LogWarning("Error: " + e.Message);
            }
        }



        void showPanelInfo(RawImage ModalImage)
        {
            cam.enabled = false;
            PanelInfo.gameObject.SetActive(true);
        }
        void showManualPanel()
        {

            ManualPanel.gameObject.SetActive(true);

        }

        void CloseManual()
        {

            ManualPanel.gameObject.SetActive(false);


        }
        void showDownloadPanel()
        {

            DownloadPanel.gameObject.SetActive(true);

        }

        void CloseDownloadPanel()
        {

            DownloadPanel.gameObject.SetActive(false);


        }
    }
}
