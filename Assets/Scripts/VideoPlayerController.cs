﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using YoutubeLight;
using UnityEngine.UI;

public class VideoPlayerController : MonoBehaviour
{
    private System.Threading.Thread m_Thread = null;

    //**  public Transform PlayButton;
    public Sprite butt_play_sprite;
    public Sprite butt_stop_sprite;

    public GameObject loading;
    public RawImage image;

    public string videoId = "";
    public string videoUrl = "";
    private bool videoAreReadyToPlay = false;
    //use unity player(all platforms) or old method to play in mobile only if you want, or if your mobile dont support the new player.
    public bool useNewUnityPlayer;
    public VideoPlayer unityVideoPlayer;
    public string unityVideoPlayerHeight;
    public string unityVideoPlayerWidth;
    public AspectRatioFitter imageFitter; 

    private VideoClip videoToPlay; // If not youtube

    //Audio
    private AudioSource audioSource;

    public bool isyoutube = false;

    private bool video_runs = true;

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();

        //**   PlayButton.GetComponent<Button>().onClick.AddListener(delegate { PlayStopVideo(); });
    }

    public void StartVideo(VideoClip current_video_clip, bool is_youtube)
    {

        isyoutube = is_youtube;

        video_runs = true;
        //**   PlayButton.GetComponent<UnityEngine.UI.Image>().sprite = butt_stop_sprite;

        loading.SetActive(true);
        //**   PlayButton.GetComponent<Button>().interactable = false;

        if (current_video_clip != null)
        {
            videoToPlay = current_video_clip;
        }
        StartCoroutine(wait(1));
    }

    IEnumerator wait(float seconds)
    {
        // Debug.Log("Before Waiting " + seconds + " seconds");
        yield return new WaitForSeconds(seconds);
        // Debug.Log("After Waiting " + seconds + " Seconds");

        audioSource.playOnAwake = false;

        if (isyoutube)
        {
            PlayYoutubeVideo(videoId);
        }
        else
        {
            StartCoroutine(playVideo());
        }

    }

    /*
     * Youtube Videos
     */

    public void PlayYoutubeVideo(string _videoId)
    {
        videoId = _videoId;
        //Call the video system in another thread
        m_Thread = new System.Threading.Thread(GetTheVideoGoTresor); //++ GetTheVideo
        m_Thread.Start();
    }

    /*
     * Youtube Videos -> this will run only in another thread.
     */

    void GetTheVideoGoTresor()
    {

        videoAreReadyToPlay = true;

    }

    /*  void GetTheVideo()
      {
          List<VideoInfo> videoInfos = RequestResolver.GetDownloadUrls(videoId, false);
          foreach (VideoInfo info in videoInfos)
          {

              Debug.Log("Play-INFO!!" + info.Resolution);
              if (info.VideoType == VideoType.Mp4 && info.Resolution == (360))
              {

                  if (info.RequiresDecryption)
                  {
                      //The string is the video url
                      videoUrl = RequestResolver.DecryptDownloadUrl(info);
                  }
                  else
                  {
                      videoUrl = info.DownloadUrl;
                  }
                  videoAreReadyToPlay = true;
                  break;
              }
          }
      }*/

    /*
     * FixedUpdate
     */

    bool checkIfVideoArePrepared = false;
    void FixedUpdate()
    {
        //used this to play in main thread.
        if (videoAreReadyToPlay)
        {
            videoAreReadyToPlay = false;
            //play using the old method
            if (!useNewUnityPlayer)
                StartCoroutine(StartVideo());
            else
            {
                //   Debug.Log("Play-FixedUpdate!!" + videoUrl);
                unityVideoPlayer.playOnAwake = false;
                unityVideoPlayer.playOnAwake = false;
                audioSource.playOnAwake = false;
                //++ unityVideoPlayer.source = VideoSource.Url;
                // videoUrl = "https://r4---sn-c0q7ln76.googlevideo.com/videoplayback?expire=1530905705&ipbits=0&dur=613.053&lmt=1458717046266517&ei=CHA_W4zGPJOGVIu1r4gM&itag=18&fvip=6&pl=16&source=youtube&c=WEB&clen=52606050&id=o-AAvZQXqwG-MgE20mGB1wgaAz-GtfJEjAesaSLicCvJAJ&sparams=clen,dur,ei,expire,gir,id,initcwndbps,ip,ipbits,ipbypass,itag,lmt,mime,mm,mn,ms,mv,nh,pl,ratebypass,requiressl,source&ip=93.139.26.7&ratebypass=yes&fexp=23709359&signature=4A94E096A0E8B8A82FF5DAF8D04335077568ED1C.6A2CBBF1235F45005DD3ABC97ECD809CE357F0D7&mime=video/mp4&gir=yes&key=cms1&requiressl=yes&redirect_counter=1&rm=sn-bvvbax-15be7d&req_id=da263eda122a3ee&cms_redirect=yes&ipbypass=yes&mm=30&mn=sn-c0q7ln76&ms=nxu&mt=1530885084&mv=m";
                unityVideoPlayer.url = videoUrl; //++

                checkIfVideoArePrepared = true;
                //Set Audio Output to AudioSource
                unityVideoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

                //Assign the Audio from Video to AudioSource to be played
                unityVideoPlayer.EnableAudioTrack(0, true);
                unityVideoPlayer.SetTargetAudioSource(0, audioSource);
                unityVideoPlayer.Prepare();
                image.texture = unityVideoPlayer.texture;
            }
        }

        if (checkIfVideoArePrepared)
        {
            checkIfVideoArePrepared = false;
            StartCoroutine(playVideoURL()); // Before - PreparingAudio
        }
    }

    /*
     * PreparingAudio and play youtube Video
     */

    IEnumerator playVideoURL()
    {

        unityVideoPlayer.Prepare();
        //Wait until video is prepared                                   
        WaitForSeconds waitTime = new WaitForSeconds(4);
        while (!unityVideoPlayer.isPrepared)
        {
            //    Debug.Log("Preparing Video");
            //Prepare/Wait for 5 sceonds only                            
            yield return waitTime;
            //Break out of the while loop after 5 seconds wait           
            break;
        }

        //   Debug.Log("Done Preparing Video");

        //   audioSource.playOnAwake = false;

        //Assign the Texture from Video to RawImage to be displayed    
        image.texture = unityVideoPlayer.texture;

        //Play Video                                                     
        unityVideoPlayer.Play();

        //Play Sound                                                     
        audioSource.Play();

        loading.SetActive(false);
        //**  PlayButton.GetComponent<Button>().interactable = true;     

        //   Debug.Log("Playing Video playVideoURL " + unityVideoPlayer.texture.height.ToString() + " - " + unityVideoPlayer.texture.width.ToString()  +" - " + ((float)unityVideoPlayer.texture.width / (float)unityVideoPlayer.texture.height));
        imageFitter.aspectRatio = (float)unityVideoPlayer.texture.width / (float)unityVideoPlayer.texture.height;
        while (unityVideoPlayer.isPlaying)
        {
            yield return null;
        }
        OnVideoFinished();

    }

    /*
     * Stop
     */

    public void Stop()
    {
        unityVideoPlayer.Stop();
        checkIfVideoArePrepared = false;

        isyoutube = false;
    }

    /*
     * StartVideo
     */

    IEnumerator StartVideo()
    {
#if UNITY_IPHONE || UNITY_ANDROID
        Handheld.PlayFullScreenMovie(videoUrl);
#endif
        yield return new WaitForSeconds(1.4f);
        OnVideoFinished();
    }

    /*
     * OnVideoFinished
     */

    public void OnVideoFinished()
    {
        Debug.Log("Video finished");
    }

    /*
     * Plays the embedded videos
     */

    IEnumerator playVideo()
    {

        //Disable Play on Awake for both Video and Audio
        unityVideoPlayer.playOnAwake = false;
        unityVideoPlayer.playOnAwake = false;

        //We want to play from video clip not from url
        unityVideoPlayer.source = VideoSource.VideoClip;
        //videoPlayer.source = VideoSource.Url;
        //videoPlayer.url = "https//www.youtube.com/watch?v=KQ63wu7lsXo";

        //Set Audio Output to AudioSource
        unityVideoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        unityVideoPlayer.EnableAudioTrack(0, true);
        unityVideoPlayer.SetTargetAudioSource(0, audioSource);

        //Set video To Play then prepare Audio to prevent Buffering
        unityVideoPlayer.clip = videoToPlay;
        unityVideoPlayer.Prepare();

        //Wait until video is prepared
        while (!unityVideoPlayer.isPrepared)
        {
            //  Debug.Log("Preparing Video");
            yield return null;
        }

        //Debug.Log("Done Preparing Video");

        //Assign the Texture from Video to RawImage to be displayed
        image.texture = unityVideoPlayer.texture;

        //Play Video
        unityVideoPlayer.Play();

        //Play Sound
        audioSource.Play();

        //Debug.Log("Playing Video playVideo " + unityVideoPlayer.texture.height.ToString() + " - " + unityVideoPlayer.texture.width.ToString() + " - " + ((float)unityVideoPlayer.texture.width / (float)unityVideoPlayer.texture.height));
        imageFitter.aspectRatio = (float)unityVideoPlayer.texture.width / (float)unityVideoPlayer.texture.height;
        unityVideoPlayerHeight = unityVideoPlayer.texture.height.ToString();
        unityVideoPlayerWidth = unityVideoPlayer.texture.width.ToString();

        loading.SetActive(false);
        //**   PlayButton.GetComponent<Button>().interactable = true;

        while (unityVideoPlayer.isPlaying)
        {
            // Debug.LogWarning("Video Time: " + Mathf.FloorToInt((float)unityVideoPlayer.time));
            yield return null;
        }

        Debug.Log("Done Playing Video");
    }

    public void PlayStopVideo()
    {

        //++  Sprite current_sprite;

        if (!video_runs)
        {

            //++  current_sprite = butt_stop_sprite;
            video_runs = true;
            unityVideoPlayer.Play();
        }
        else
        {

            //++  current_sprite = butt_play_sprite;
            video_runs = false;
            unityVideoPlayer.Pause();

        }

        //**   PlayButton.GetComponent<UnityEngine.UI.Image>().sprite = current_sprite;
    }
}
