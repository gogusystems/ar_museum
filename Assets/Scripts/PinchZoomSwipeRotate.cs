﻿/*
 *	PinchZoomSwipeRotate: mobile device control to scale and rotate GameObject In the scene
 *	Modified by Tobias Hörl
 */
using UnityEngine;

namespace GoguSystems
{

    public class PinchZoomSwipeRotate : MonoBehaviour
    {
        private const float maxScale = 1.40f;
        private const float minScale = 0.40f;

        private const float scaleValueMultiplier = 0.005f;
        private const float rotationMultiplier = 1.25f;

        public float perspectiveZoomSpeed = 1.0f;        // The rate of change of the field of view in perspective mode.
        public float orthoZoomSpeed = 1.0f;        // The rate of change of the orthographic size in orthographic mode.

        public GameObject elementToZoom;

        void Update()
        {
            // Rotation on swipe
            if (Input.touchCount == 1)
            {
                if (Input.touches.Length > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    elementToZoom.transform.rotation *= Quaternion.AngleAxis(Input.GetTouch(0).deltaPosition.y * rotationMultiplier, Vector3.up);
                    //elementToZoom.transform.Rotate(Vector3.forward * rotationMultiplier * Time.deltaTime); 
                }
            }

            // Zoom on Pinch
            if (Input.touchCount == 2)
            {
                // Store both touches.
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                // Find the position in the previous frame of each touch.
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                // Find the magnitude of the vector (the distance) between the touches in each frame.
                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                // Find the difference in the distances between each frame.
                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                float currentscale = elementToZoom.transform.localScale.x;

                float scaleDelta = 0.0f;

                if (Camera.main.orthographic)
                {
                    scaleDelta = -(deltaMagnitudeDiff * orthoZoomSpeed * scaleValueMultiplier);
                }
                else
                {
                    scaleDelta = -(deltaMagnitudeDiff * perspectiveZoomSpeed * scaleValueMultiplier);
                }

                if ((scaleDelta > 0.0f) && ((currentscale + scaleDelta) <= maxScale))
                {
                    elementToZoom.transform.localScale += (Vector3.one * scaleDelta);
                }
                if ((scaleDelta < 0.0f) && ((currentscale + scaleDelta) >= minScale))
                {
                    elementToZoom.transform.localScale += (Vector3.one * scaleDelta);
                }

            }
        }
    }

}